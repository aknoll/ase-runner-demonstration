[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Faknoll%2Fase-runner-demonstration.git/main)

# ASE RuNNer Demonstration

Demonstrate the usage of the new Runner calculator and FileIO routines in ASE. 
